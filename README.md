japptools
=========

Software component usable as black-box. Provide features:

* Read a configuration file in **xml format** as **Java object**. This file can contains configuration for RDBMS, LDAP, email servers and JMS communication channels (Topic & Queue).
* Support **encrypted password storage** in configuration file.
* Very easy **send mail** via configured email server. Only, provides subject and body to method call.

For more details visit (http://www.meddeb.net/japptools).
