package net.meddeb.japptools.test;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import net.meddeb.japptools.JApptoolsPin;
import net.meddeb.japptools.Serverconf;
import junit.framework.TestCase;

public class ToolsTest extends TestCase {

	public ToolsTest() {
		
	}

	public ToolsTest(String name) {
		super(name);
	}
	
	public void testEncryption(){
		String str = "ToolsTest";
		String encStr = "";
		JApptoolsPin apptoolsPin = JApptoolsPin.getInstance();
		encStr = apptoolsPin.encrypt(str);
		assertEquals(true, !encStr.isEmpty());
		str = apptoolsPin.getKey(90);
		assertEquals(str.length(), 90);
	}
	
	public void testConfig(){
		JApptoolsPin apptoolsPin = JApptoolsPin.getInstance();
		Serverconf serverConf = null;
		// Init config with default values
		assertEquals(false,apptoolsPin.initConfig());
		// retunr nuul when not initialized
		assertEquals(null,apptoolsPin.getServerconf("oracleServer"));
		//Init config with supplied values
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.substring(0, path.lastIndexOf(System.getProperty("file.separator"))+1);
		assertEquals(true,apptoolsPin.initConfig(path, "config-example.xml"));
		// Init log
		assertEquals(true,apptoolsPin.initLog());
		//Oracle RDBMS config
		serverConf = apptoolsPin.getServerconf("oracleServer");
		assertEquals("jdbc:oracle:thin:@oracle.domain.com:1526:oracleSID", serverConf.getConnectionUrl());
		assertEquals("oracleUser", serverConf.getLogin());
		assertEquals("oraclePassword", serverConf.getPassword());
		//PostgreSQL RDBMS config
		serverConf = apptoolsPin.getServerconf("postgresqlServer");
		assertEquals("jdbc:postgresql://postgresql.domain.com:5432:/postgresqlSID", serverConf.getConnectionUrl());
		assertEquals("postgresqlSID", serverConf.getSID());
		assertEquals("postgresqlUser", serverConf.getLogin());
		assertEquals("postgresqlPassword", serverConf.getPassword());
		//LDAP Server config
		serverConf = apptoolsPin.getServerconf("ldapServer");
		assertEquals("ldap.domain.com", serverConf.getHost());
		assertEquals("ldap://ldap.domain.com", serverConf.getConnectionUrl());
		assertEquals("389", serverConf.getPort());
		assertEquals("dc=domain,dc=com", serverConf.getSID());
		assertEquals("uid=manager,dc=domain,dc=com", serverConf.getLogin());
		assertEquals("ldapPassword", serverConf.getPassword());
		//JMS Chnnels config
		serverConf = apptoolsPin.getServerconf("jmsChannels");
		assertEquals("jms.domain.com", serverConf.getHost());
		assertEquals("61616", serverConf.getPort());
		assertEquals("jmsUser", serverConf.getLogin());
		assertEquals("jmsPassword", serverConf.getPassword());
		//Mail server config
		serverConf = apptoolsPin.getServerconf("mailServer");
		assertEquals("mail.domain.com", serverConf.getHost());
		assertEquals("smtp://mail.domain.com", serverConf.getConnectionUrl());
		assertEquals("25", serverConf.getPort());
	}

}
