package net.meddeb.japptools;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

public class Entrypoint {

	private static String getDelim(int len){
		if (len > 150) len = 150;
		if (len < 10) len = 10;
		String rslt = new String(new char[len]).replace("\0", "-");
		return rslt;
	}
	
	private static void showMessageHeader(){
		System.out.println("");
		System.out.println("--------------------------------------------------------------------");
		System.out.println("japptools, Java application tools - Version " + JApptoolsPin.VERSION);
		System.out.println("Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)");
    System.out.println("This program is free software and comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("--------------------------------------------------------------------");
		System.out.println("");
	}
	
	private static void showHelp(){
		showMessageHeader();
		System.out.println("Usage");
		System.out.println("");
		System.out.println("java -jar majapptools-"+JApptoolsPin.VERSION+"-stdone.jar -h : show this help");
		System.out.println("java -jar majapptools-"+JApptoolsPin.VERSION+"-stdone.jar -k <keyLength> : generate a random encryption key");
		System.out.println("java -jar majapptools-"+JApptoolsPin.VERSION+"-stdone.jar -e <TextToEncrypt>: encrypt supplied text");
		System.out.println("");
	}
	
	private static void showWrongParams(){
		showMessageHeader();
		System.out.println("Uknown parameter, use -h for help");
		System.out.println("");
	}

	private static void showWrongKLength(){
		showMessageHeader();
		System.out.println("Wrong argument for -k parameter");
		System.out.println("");
	}
	
	private static void genKey(int length){
		Encryption enc = new Encryption();
		System.out.println("");
		System.out.println("Key generated: " + length + " characters length.");
		String delim = getDelim(length);
		System.out.println(delim);
    System.out.println(enc.getKey(length));
		System.out.println(delim);
		System.out.println("");
	}
	
	private static void encrypt(String str){
		Encryption enc = new Encryption();
		String eStr = enc.encrypt(str);
		System.out.println("");
		System.out.println("Encrypted text.");
		String delim = getDelim(eStr.length());
		System.out.println(delim);
    System.out.println(eStr);
		System.out.println(delim);
		System.out.println("");
	}
	
	public Entrypoint() {
	}

	public static void main(String[] args) {
		switch (args.length){
			case 0: 
				showHelp();
				break;
			case 1:
				if ((args[0].equalsIgnoreCase("--help"))||(args[0].equalsIgnoreCase("-h"))){
					showHelp();
				} else showWrongParams(); 
				break;
			case 2:
				if (args[0].equalsIgnoreCase("-k")){
					String strLength = args[1];
					int kLength = -1;
					try{
						kLength = Integer.parseInt(strLength);
					} catch (NumberFormatException e){
					}
					if (kLength <= 0){
						showWrongKLength();
					}
					else genKey(kLength);
				} else if (args[0].equalsIgnoreCase("-e")){
					encrypt(args[1]);
				} else showWrongParams(); 
				break;
			default:
				showWrongParams();
				break;
		}
	}

}
