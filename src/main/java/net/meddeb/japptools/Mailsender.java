package net.meddeb.japptools;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import org.apache.log4j.Logger;

class Mailsender {
	private static final String defaultSubject = "No subject";
	private Logger logger = Logger.getLogger(this.getClass());
	private String host = "";
	private String port = "";
	private ArrayList<String> bodyList;
	private ArrayList<String> mailTOList;
	private ArrayList<String> mailCCList;
	private String from = "";
	private String subject = "";
	private MimeMessage message = null;
	private String contentType = "text/plain;charset=utf-8";
	private String getBody(){
		String rslt = "";
		for (int i=0;i<bodyList.size();i++){
			rslt = rslt + bodyList.get(i) + "\n";
		}
		return rslt;
	}
	private void initMessage(){
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", host);
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.mime.charset", "utf-8");
		props.put("mail.mime.encodeeol.strict", "true");
		props.setProperty("mail.smtp.quitwait", "false");

		Session session = Session.getDefaultInstance(props,
		new Authenticator(){
		  protected PasswordAuthentication getPasswordAuthentication(){ 
			  return null;
		  }
		});
		message = new MimeMessage(session);
		try{
			message.setHeader("Content-Type",contentType);
			message.addHeader("Auto-Submitted", "auto-generated");
			message.setSender(new InternetAddress(from));
		}
		catch (MessagingException e){
			message = null;
			session = null;
			logger.error(e.getMessage());
		}
	}
	public Mailsender(String host, String port){
		this.host = host;
		this.port = port;
		bodyList = new ArrayList<String>();
		mailTOList = new ArrayList<String>();
		mailCCList = new ArrayList<String>();
	}
	public boolean sendMail(){
		if ((mailTOList.size() == 0) && (mailCCList.size() == 0)){
			logger.error("Cannot send mail, recipients list is empty.");
			return false;
		}
		try {
			initMessage();
			if (subject.equalsIgnoreCase(defaultSubject)) logger.warn("Send mail with no subject defined.");
			if (bodyList.size() == 0) logger.warn("Send mail with no body defined.");
			if (from.isEmpty()) logger.warn("Send mail with no from sender defined.");
			System.out.println("Message: " + message);
			message.setSubject(subject, "utf-8");
			message.setContent(getBody(), contentType);
			for (int i=0; i<mailTOList.size(); i++){
				if (i == 0){
					message.setRecipient(Message.RecipientType.TO, new InternetAddress(mailTOList.get(i))); 
				}
				else message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailTOList.get(i)));
			}
			for (int i=0; i<mailCCList.size(); i++){
				if (i == 0){
					message.setRecipient(Message.RecipientType.CC, new InternetAddress(mailCCList.get(i))); 
				}
				else message.addRecipient(Message.RecipientType.CC, new InternetAddress(mailCCList.get(i)));
			}
			logger.debug("Sending mail..");
			Transport.send(message);	
			logger.debug("Mail succefully sent.");
			message = null;
			return true;
		}
		catch (MessagingException e){
			message = null;
			logger.error(e.getMessage());
			return false;
		}
	}
	
	public void setSubject(String strSubject){
		if ((strSubject != null) && (!strSubject.isEmpty())){
			subject = strSubject;
		}
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public void addBody(String strLine){
		bodyList.add(strLine);
	}
	
	public void clearBody(){
		bodyList.clear();
	}
	
	public void addMailto(String strLine){
		mailTOList.add(strLine);
	}
	
	public void clearMailto(){
		mailTOList.clear();
	}
	
	public void addMailcc(String strLine){
		mailCCList.add(strLine);
	}
	
	public void clearMailcc(){
		mailCCList.clear();
	}
	
}
