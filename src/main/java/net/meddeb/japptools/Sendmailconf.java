package net.meddeb.japptools;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.util.ArrayList;

class Sendmailconf {
	private String mailFrom = "";
	private ArrayList<String> mailtoList = null;
	private ArrayList<String> mailccList = null;
	
	public Sendmailconf() {
		mailtoList = new ArrayList<String>();
		mailccList = new ArrayList<String>();
	}
	
	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public void addMailto(String to){
		mailtoList.add(to);
	}
	
	public String getMailto(int idx){
		return mailtoList.get(idx);
	}
	
	public void clearMailto(){
		mailtoList.clear();
	}
	
	public int mailtoSize(){
		return mailtoList.size();
	}
	
	public void addMailcc(String cc){
		mailccList.add(cc);
	}
	
	public String getMailcc(int idx){
		return mailccList.get(idx);
	}
	
	public void clearMailcc(){
		mailccList.clear();
	}
	
	public int mailccSize(){
		return mailccList.size();
	}

}
