package net.meddeb.japptools;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

class ConfigFile {
	private static final String DEFAULT_LOGFILE = "log4j.xml";
	private static final String DEFAULT_CONFFILE = "config.xml";
	private Logger logger = null;
	private String configPathname = "";
	private String configFilename = "";
	private String logPathname = "";
	private String logFilename = "";
	private String fqFilename = ""; //full qualified config file name
	private String pathSeparator = "/"; //OS dependant
	private boolean ready;
	private Document xmlDoc = null;
	private Encryption encryptor = null;
	
	private String addTrailingSeparator(String path){
		String rslt = path;
		if (path.charAt(path.length()-1) != pathSeparator.charAt(0)){
			rslt = rslt + pathSeparator;
		}
		return rslt;
	}
	
	private boolean isFileExists(String fullFilename){
		boolean rslt = (fullFilename != null) && (!fullFilename.isEmpty());
		if (rslt){
			File f = new File(fullFilename);
			rslt = (f.exists()) && (f.isFile());
		}
		return rslt;
	}
	
  private String getLocalpath(){
  	String strPath = "";
  	strPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
  	if ((strPath != null) && (!strPath.isEmpty())){
    	File pathFile = new File(strPath);
      if (pathFile.isFile()) { //runtime, jar file
      	int idxEnd = strPath.lastIndexOf(pathSeparator);
        strPath = strPath.substring(0,idxEnd);
        strPath = addTrailingSeparator(strPath);
      }
      else if (pathFile.isDirectory()){ //dev time, class file out of a jar
      	strPath = addTrailingSeparator(strPath);
      }
  	}
    return strPath;
  }

  private void initXmldoc(){
		if (!ready) return;
		try {
			if (xmlDoc == null){
				DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
				xmlDoc = docBuilder.parse (new File(fqFilename));
				xmlDoc.getDocumentElement().normalize();
			}
		} catch (SAXParseException pe) {
			logger.error("Configuration file structure error, line " + pe.getLineNumber () + ", uri " + pe.getSystemId ());
    } catch (Exception e) {
			logger.error("Error occured when initializing configuration file: " + e.getMessage());
		}
	}

  public ConfigFile(){
		pathSeparator = System.getProperty("file.separator");
		encryptor = new Encryption();
	}
	
	public boolean initConfig(){
		this.configPathname = getLocalpath();
		fqFilename = configPathname + DEFAULT_CONFFILE;
		if (logger != null) logger.debug("Configuration file set to " + fqFilename);
		ready = isFileExists(fqFilename);
		if ((!ready)&&(logger!=null)) logger.debug("Configuration file not found, using default settings");
		return ready;
	}
	
	public boolean initConfig(String configPath){
		this.configPathname = configPath;
		fqFilename = configPath + DEFAULT_CONFFILE;
		if (logger != null) logger.debug("Configuration file set to " + fqFilename);
		ready = isFileExists(fqFilename);
		if ((!ready)&&(logger!=null)) logger.debug("Configuration file not found, using default settings");
		return ready;
	}
	
	public boolean initConfig(String configPath, String configFile){
		this.configPathname = configPath;
		fqFilename = configPath + configFile;
		if (logger != null) logger.debug("Configuration file set to " + fqFilename);
		ready = isFileExists(fqFilename);
		if ((!ready)&&(logger!=null)) logger.debug("Configuration file not found, using default settings");
		return ready;
	}
	
	public boolean initLog(){
		String logFilename = "";
		boolean rslt = false;
		if (!configPathname.isEmpty()){
			logFilename = configPathname + DEFAULT_LOGFILE;
			rslt = isFileExists(logFilename);
		}
		if (!rslt){
			logFilename = getLocalpath() + DEFAULT_LOGFILE;
			rslt = isFileExists(logFilename);
		}
		if (rslt){
			DOMConfigurator.configure(logFilename);
			logger = Logger.getLogger(this.getClass());
		}
		return rslt;
	}
	
	public boolean initLog(String logFilepath){
		String logFilename = "";
		boolean rslt = false;
		if ((logFilepath != null) && (!logFilepath.isEmpty())){
			logFilename = logFilepath + DEFAULT_LOGFILE;
			rslt = isFileExists(logFilename);
		}
		if (!rslt){
			logFilename = getLocalpath() + DEFAULT_LOGFILE;
			rslt = isFileExists(logFilename);
		}
		if (rslt){
			DOMConfigurator.configure(logFilename);
			logger = Logger.getLogger(this.getClass());
		}
		return rslt;
	}
	
	public Serverconf getServerconf(String serverID){
		if (!ready) return null;
		String strType = "";
		String strVendor = "";
    Serverconf rslt = null;
		Node childNode = null;
		NodeList childNodeList = null;
		Element serverElement = null;
		ServerParams sParams = new ServerParams();
		initXmldoc();
		NodeList nodeList = xmlDoc.getElementsByTagName(ServerParams.SERVER_TAG);
		for (int i=0; i< nodeList.getLength(); i++){
			childNode = nodeList.item(i);
			if ((childNode instanceof Element)&&
					(childNode.getAttributes().getNamedItem("id").getTextContent().equalsIgnoreCase(serverID))){
				serverElement = (Element)childNode;
				break;
			}
		}
		strType = serverElement.getAttributes().getNamedItem("type").getTextContent();
		if (sParams.getServerType(strType) == ServerParams.ServerType.RDBMS){
			strVendor = ((Element)childNode).getAttributes().getNamedItem("vendor").getTextContent();
		}
		childNodeList = childNode.getChildNodes();
		if (childNodeList != null){
			String host = "";
			String sid = "";
			String port = "";
			String login = "";
			String passwd = "";
			String encryptedPasswd = "";
			for(int i=0; i<childNodeList.getLength() ; i++){
				childNode = childNodeList.item(i);
				if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
					(childNode.getNodeName().equals("Host"))){
					host = ((Element)childNode).getTextContent().trim();
				}
				if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
						(childNode.getNodeName().equals("Sid"))){
						sid = ((Element)childNode).getTextContent().trim();
					}
				if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
					(childNode.getNodeName().equals("Port"))){
					port = ((Element)childNode).getTextContent().trim();
				}
				if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
						(childNode.getNodeName().equals("Login"))){
					login = ((Element)childNode).getTextContent().trim();
				}
				if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
						(childNode.getNodeName().equals("Password"))){
					passwd = ((Element)childNode).getTextContent().trim();
					encryptedPasswd = ((Element)childNode).getAttribute("encrypted");
				}
			}
			if (!host.isEmpty()){
				logger.debug("Configuration loaded for " + serverID);
				logger.debug("  ** Host     : " + host);
				logger.debug("  ** Port     : " + port);
				logger.debug("  ** Login    : " + login);
				logger.debug("  ** Password : " + passwd);
				rslt = new Serverconf(host);
				rslt.setSID(sid);
				rslt.setPort(port);
				if (!login.isEmpty()) rslt.setLogin(login);
				if (!passwd.isEmpty()){
					boolean passwdIsEncrypeted = ((encryptedPasswd == null)||(encryptedPasswd.isEmpty())||(encryptedPasswd.equalsIgnoreCase("true")));
					if (passwdIsEncrypeted) passwd = encryptor.decrypt(passwd);
					rslt.setPassword(passwd);
				}
				rslt.setConnectionUrl(sParams.getConnectionUrl(strType, strVendor, rslt));
			}
		}
    return rslt;
	}
	
	public Sendmailconf getSendmailconf(String serverID){
		if (!ready) return null;
		String strType = "";
    Sendmailconf rslt = new Sendmailconf();
		Node childNode = null;
		NodeList childNodeList = null;
		Element serverElement = null;
		Element destinationElement = null;
		ServerParams sParams = new ServerParams();
		initXmldoc();
		NodeList nodeList = xmlDoc.getElementsByTagName(ServerParams.SERVER_TAG);
		for (int i=0; i< nodeList.getLength(); i++){
			childNode = nodeList.item(i);
			if ((childNode instanceof Element)&&
					(childNode.getAttributes().getNamedItem("id").getTextContent().equalsIgnoreCase(serverID))){
				serverElement = (Element)childNode;
				break;
			}
		}
		strType = serverElement.getAttributes().getNamedItem("type").getTextContent();
		if (sParams.getServerType(strType) == ServerParams.ServerType.MAIL){
			nodeList = serverElement.getElementsByTagName("Destination");
			if (nodeList != null){
				for (int i=0; i< nodeList.getLength(); i++){
					childNode = nodeList.item(i);
					if (childNode instanceof Element){
						destinationElement = (Element)childNode;
						break;
					}
				}
			}
			NodeList itemNodes = null;
			if (destinationElement != null){
				childNodeList = destinationElement.getChildNodes();
				if (childNodeList != null){
					for(int i=0; i<childNodeList.getLength() ; i++){
						childNode = childNodeList.item(i);
						if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
							(childNode.getNodeName().equals("MailFrom"))){
							rslt.setMailFrom(((Element)childNode).getTextContent().trim());
						}
						if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
								(childNode.getNodeName().equals("MailTO"))){
							itemNodes = childNode.getChildNodes();
							for(int j=0; j<itemNodes.getLength() ; j++){
								childNode = itemNodes.item(j);
								if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
										(childNode.getNodeName().equals("Item"))){
									rslt.addMailto(((Element)childNode).getTextContent().trim());
								}
							}
						}
						if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
							(childNode.getNodeName().equals("MailCC"))){
							itemNodes = childNode.getChildNodes();
							for(int j=0; j<itemNodes.getLength() ; j++){
								childNode = itemNodes.item(j);
								if ((childNode.getNodeType() == Node.ELEMENT_NODE) &&
										(childNode.getNodeName().equals("Item"))){
									rslt.addMailcc(((Element)childNode).getTextContent().trim());
								}
							}
						}
					}
				}
			}
		}
    return rslt;
	}

	public boolean isReady() {
		return ready;
	}

	public String getConfigPathname() {
		return configPathname;
	}

	public void setConfigPathname(String configPathname) {
		this.configPathname = configPathname;
	}

	public String getConfigFilename() {
		return configFilename;
	}

	public void setConfigFilename(String configFilename) {
		this.configFilename = configFilename;
	}

	public String getLogPathname() {
		return logPathname;
	}

	public void setLogPathname(String logPathname) {
		this.logPathname = logPathname;
	}

	public String getLogFilename() {
		return logFilename;
	}

	public void setLogFilename(String logFilename) {
		this.logFilename = logFilename;
	}
	
}
