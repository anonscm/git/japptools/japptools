package net.meddeb.japptools;

import java.util.ArrayList;

import org.apache.log4j.Logger;

/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

 /** 
 * \english
 * Package facade class
 * This is the single entry point for japptools component
 * \endenglish
 * \french
 * Classe façade pour le package
 * Il s'agit de l'unique point d'entrée du composant japptools
 * \endfrench
 *
 */
final public class JApptoolsPin {
	public static final String VERSION = "1.1-SNAPSHOT";
	private Logger logger = Logger.getLogger(this.getClass());
	private Encryption encryption = null;
	private ConfigFile configFile = null;
	private static JApptoolsPin instance = null;
	protected JApptoolsPin() {
		configFile = new ConfigFile();
		encryption = new Encryption();
	}
	/**
 * \english
 * Create the single JApptoolsPin class instance (singleton)
 * @return The single JApptoolsPin class instance
 *  \endenglish
 *  \french
 * Crée l'unique instance de la classe JApptoolsPin (singleton)
 * @param keyLength la longueur de la clé demandée
 * @return L'unique instance de lma classe JApptoolsPin
 *  \endfrench
 */
	public static JApptoolsPin getInstance(){
		if (instance == null){
			instance = new JApptoolsPin();
		}
		return instance;
	}
	/**
 * \english
 * Generate random encryption key
 * @param keyLength requested key length
 * @return Random encryption key
 *  \endenglish
 *  \french
 * Génère une clé aléatoire pour le cryptage
 * @param keyLength la longueur de la clé demandée
 * @return Clé aléatoire de cryptage
 *  \endfrench
 */
	public String getKey(int keyLength){
		return encryption.getKey(keyLength);
	}
	/**
 * \english
 * Encrypt a text
 * @param str text to encrypt
 * @return Encrypted text
 *  \endenglish
 *  \french
 * Crypte un texte
 * @param str texte à crypter
 * @return Texte crypté
 *  \endfrench
 */
	public String encrypt(String str){
		return encryption.encrypt(str);
	}
	/**
 * \english
 * Load configuration file
 * At least one initConfig method must be called before any request for server configuration or send mail
 * This method assume default configuration file usage: confgi.xml. It assume also that this file and log4j.xml, if used, are stored
 * in the same folder than jar package.
 * @return boolean true if initialization successed
 *  \endenglish
 *  \french
 * Lit le fichier de configuration
 * Au moins une des methodes initConfig doit être appelée avant toute demande de récupération d'une configuration d'un serveur ou d'envoi 
 * de courriel. Cette methode utilise le nom de fichier de configuration par défaut: config.xml. Elle considère, également,  que ce fichier
 * et le fichier log4j.xml, s'il est utilisé, sont stockés dans le dossier de stockage du fichier jar du package.
 * @return booléen true si l'initialisation a réussi
 *  \endfrench
 */
	public boolean initConfig(){
		return configFile.initConfig();
	}
	/**
 * \english
 * Load configuration file
 * At least one initConfig method must be called before any request for server configuration or send mail
 * This method assume default configuration file usage: confgi.xml. It assume also that this file and log4j.xml, if used, are stored
 * in the confPath folder.
 * @param confPath the full qualified path to folder where stored configuration file config.xml and log4j.xml file if used.
 * @return boolean true if initialization successed
 *  \endenglish
 *  \french
 * Lit le fichier de configuration
 * Au moins une des methodes initConfig doit être appelée avant toute demande de récupération d'une configuration d'un serveur ou d'envoi 
 * de courriel. Cette methode utilise le nom de fichier de configuration par défaut: config.xml. Elle considère, également,  que ce fichier
 * et le fichier log4j.xml, s'il est utilisé, sont stockés dans le dossier confPath.
 * @param confPath Chemin complet du dossier de stockage des fichiers de configuration config.xml et du fichier log4j.xml si utilisé.
 * @return booléen true si l'initialisation a réussi
 *  \endfrench
 */
	public boolean initConfig(String confPath){
		return configFile.initConfig(confPath);
	}
	/**
 * \english
 * Load configuration file
 * At least one initConfig function must be called before any request for server configuration or send mail
 * @param confPath the full qualified path to folder where stored configuration file and log4j.xml file if used.
 * @param confFilename configuration file name other than config.xml (default).
 * @return boolean true if initialization successed
 *  \endenglish
 *  \french
 * Lit le fichier de configuration
 * Au moins une des methodes initConfig doit être appelée avant toute demande de récupération d'une configuration d'un serveur ou d'envoi de courriel
 * @param confPath Chemin complet du dossier de stockage des fichiers de configuration et du fichier log4j.xml si utilisé.
 * @param confFilename nom du fichier de configuration si autre que config.xml (defaut).
 * @return booléen true si l'initialisation a réussi
 *  \endfrench
 */
	public boolean initConfig(String confPath, String confFilename){
		return configFile.initConfig(confPath, confFilename);
	}
	/**
 * \english
 * Initialize logging system with log4j framework.
 * At least one initConfig method must be called before any request for server configuration or send mail
 * This method assume that default configuration file name log4j.xml is used and this file is stored in the same folder than configuration file
 * @see initConfig
 * @return boolean true if initialization successed
 *  \endenglish
 *  \french
 * Initiallise le système de journalisation avec le framework log4j.
 * Cette methode utilise le nom du fichier de configuration est celui par défaut: log4j.xml et que ce fichier est stocké dans 
 * le même dossier que celui du fichier de configuration.
 * @see initConfig
 * @return booléen true si l'initialisation a réussi
 *  \endfrench
 */
	public boolean initLog(){
		return configFile.initLog();
	}
	/**
 * \english
 * Get a server configuration
 * @param serverID server ID in the configuration file
 * @return Serverconf if call success
 * \see Serverconf
 *  \endenglish
 *  \french
 * Donne une configuration de serveur
 * @param serverID Identifiant du serveur dans le fichier de configuration
 * @return Serverconf si l'appel réussi
 * \see Serverconf
 *  \endfrench
 */
	public Serverconf getServerconf(String serverID){
		return configFile.getServerconf(serverID);
		
	}
	/**
 * \english
 * Send an email
 * @param subject the email subject
 * @param body the email body, list of strings
 * @param serverID server ID in the configuration file, must be a mail server
 * @return boolean true if email send success
 *  \endenglish
 *  \french
 * Envoi un courriel
 * @param subject sujet du courriel
 * @param body corps du courriel, liste de string
 * @param serverID identifiant du serveur dans le fichier de configuration, doit être un serveur de mail
 * @return boolean true si l'envoi réussi
 *  \endfrench
 */
	public boolean sendMail(String subject, ArrayList<String> body, String serverID){
		boolean rslt = false;
		if (!configFile.isReady()){
			logger.warn("Send mail requested when configuration not initialized.");
			return rslt;
		}
		Serverconf mailServerconf = configFile.getServerconf(serverID);
		Sendmailconf sendMailconf = configFile.getSendmailconf(serverID);
		if (sendMailconf.mailtoSize() > 0){
			Mailsender mailSender = new Mailsender(mailServerconf.getHost(), mailServerconf.getPort());
			mailSender.setFrom(sendMailconf.getMailFrom());
			mailSender.setSubject(subject);
			for (int i=0; i<sendMailconf.mailtoSize(); i++){
				mailSender.addMailto(sendMailconf.getMailto(i));
			}
			for (int i=0; i<sendMailconf.mailccSize(); i++){
				mailSender.addMailcc(sendMailconf.getMailcc(i));
			}
			if (body.size() == 0) logger.warn("Send mail with empty message body.");
			for (int i=0; i<body.size(); i++){
				mailSender.addBody(body.get(i));
			}
			rslt = mailSender.sendMail();
		} else logger.error("An attempt to send mail when receiver list is empty.");
		return rslt;
	}

}
