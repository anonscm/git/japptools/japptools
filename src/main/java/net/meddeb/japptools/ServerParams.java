package net.meddeb.japptools;

class ServerParams {
	public static enum ServerType {
		RDBMS, LDAP, MAIL, JMS, UNKNOWN
	}
	
	public static enum RDBMSVendor {
		POSTGRESQL, ORACLE, UNKNOWN
	}

	private RDBMSVendor getRDBMSVendor(String serverVendor){
		RDBMSVendor rslt = RDBMSVendor.UNKNOWN;
		if (serverVendor.equalsIgnoreCase("POSTGRESQL")){
			rslt = RDBMSVendor.POSTGRESQL;
		} else if (serverVendor.equalsIgnoreCase("ORACLE")){
			rslt = RDBMSVendor.ORACLE;
		}
		return rslt;
	}
	
	private String getRDBMSConnectionUrl(RDBMSVendor vendor, String host, String port, String sid){
		String rslt = "";
		switch (vendor){
			case POSTGRESQL:
				rslt = "jdbc:postgresql://" + host + ":" + port + ":/" + sid;
				break;
			case ORACLE:
				rslt = "jdbc:oracle:thin:@" + host + ":" + port + ":" + sid;
				break;
			default:
				rslt = "";
				break;
		}
		return rslt;
	}
	
	public static final String SERVER_TAG = "Server";

	public ServerParams() {
	}
	
	public ServerType getServerType(String serverType){
		ServerType rslt = ServerType.UNKNOWN;
		if (serverType.equalsIgnoreCase("RDBMS")){
			rslt = ServerType.RDBMS;
		} else if (serverType.equalsIgnoreCase("LDAP")){
			rslt = ServerType.LDAP;
		} else if (serverType.equalsIgnoreCase("JMS")){
			rslt = ServerType.JMS;
		} else if (serverType.equalsIgnoreCase("MAIL")){
			rslt = ServerType.MAIL;
		}
		return rslt;
	}
	
	public String getStrServerType(ServerType serverType){
		String rslt = "";
		switch (serverType){
			case RDBMS:
				rslt = "RDBMS";
				break;
			case LDAP:
				rslt = "LDAP";
				break;
			case JMS:
				rslt = "JMS";
				break;
			case MAIL:
				rslt = "MAIL";
				break;
			default:
				rslt = "UNKNOWN";
				break;
		}
		return rslt;
	}
	
	public String getConnectionUrl(String srvType, String srvVendor, Serverconf srvConf){
		String rslt = "";
		switch (getServerType(srvType)){
			case RDBMS:
				rslt = getRDBMSConnectionUrl(getRDBMSVendor(srvVendor), srvConf.getHost(), srvConf.getPort(), srvConf.getSID());
				break;
			case LDAP:
				rslt = "ldap://" + srvConf.getHost();
				break;
			case MAIL:
				rslt = "smtp://" + srvConf.getHost();
				break;
			case JMS:
				rslt = "tcp://" + srvConf.getHost() + ":" + srvConf.getPort();
				break;
			default:
				rslt = "";
				break;
		}
		return rslt;
	}

}
