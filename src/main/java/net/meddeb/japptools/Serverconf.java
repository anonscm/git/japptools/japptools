package net.meddeb.japptools;
/*--------------------------------------------------------------------
japptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/
/** 
* \english
* Server configuration readed from configuration file
* \endenglish
* \french
* Contient la configuration d'un serveur lue dan le fichier de configuration
* \endfrench
*
*/

public class Serverconf {
	private String host = "";
	private String SID = "";
	private String port = "";
	private String login = "";
	private String password = "";
	private String connectionUrl = "";
	
	public Serverconf(String host){
		this.host = host;
	}
	/**
 * \english
 * Server port
 * @return String server port
 *  \endenglish
 *  \french
 * Port d'écoute du serveur
 * @return String port du serveur
 *  \endfrench
 */
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	/**
 * \english
 * Host name
 * @return String host name
 *  \endenglish
 *  \french
 * hostanme/adresse du serveur
 * @return String adresse du serveur
 *  \endfrench
 */
	public String getHost() {
		return host;
	}
	/**
 * \english
 * SID parameter for RDBMS server or Base DN for LDAP server, nothing for mail server or JMS channels
 * @return String SID/Base DN
 *  \endenglish
 *  \french
 * Nom de l'instance pour un serveur de base de données, "base dn" pour un serveur LDAP, rien pour un serveur de courriel ou canal JMS 
 * @return String SID/Base DN
 *  \endfrench
 */
	public String getSID() {
		return SID;
	}

	public void setSID(String sID) {
		SID = sID;
	}
	/**
 * \english
 * User login
 * @return String login connection
 *  \endenglish
 *  \french
 * identifiant de l'utilisateur 
 * @return String identifiant de connexion
 *  \endfrench
 */
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	/**
 * \english
 * Plain text password
 * @return String connection password
 *  \endenglish
 *  \french
 * Mot de passe en texte claire
 * @return String mot de passe de connexion
 *  \endfrench
 */
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	/**
 * \english
 * URL of server connection. This URL is compaliant with server type (RDBMS, LDAP..) and vendor (PostgreSQL, Oracle)
 * @return String URL connection
 *  \endenglish
 *  \french
 * URL de connxion au serveur. Cette URL est compatible avec le type (RDBMS, LDAP..) et la nature du serveur (PostgreSQL, Oracle)
 * @return String URL de connexion
 *  \endfrench
 */
	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}
}
