package net.meddeb.japptools;
/*--------------------------------------------------------------------
majapptools, Java applications tools
Software component provide a set of tools that help to build application.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.jasypt.util.text.StrongTextEncryptor;

class Encryption {


	private static final String ASCIICHARS = "012ABCD4abcd!\"#345EFGHefgh$&%67IJKLMijklm'()89INOPQRSTUVWXYZnopqrstuvwxyz*+,-./:;<=@?>[\\]_^`{|}~";
	private static final String KEYFILENAME = "majapptools.dat";
	private static final String DEFAULTKEY = "4UI8bYi4>]l!VI6?X0X5iwz3^L^n@B+vQC8?MtEJRBnK!Y$[!mpd1XAVA3Gq%ZYg_mx(WEV:,Sb&tqZe/k^ji[NkOusY@7'|$A<O7#lgR#vq.5FhGp0stL~sftePh4zU"; //never change it
	private static Random rnd = new Random();
	private Logger logger = Logger.getLogger(this.getClass());
	private StrongTextEncryptor textEncryptor;
	/**
	 * @param len
	 * <span class="en">
	 * Generate a random ascii string
	 * </span>
	 * <span class="fr">
	 * Génère un mot composé de caractères ascii de manière aléatoire
	 * </span>
	 */
	private String randomString(int len){
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ){
			sb.append( ASCIICHARS.charAt( rnd.nextInt(ASCIICHARS.length()) ) );
		}
		return sb.toString();
	}
	
	private String streamToString(InputStream is) {
    Scanner scanner = new Scanner(is);
		Scanner s = scanner.useDelimiter("\\A");
    String rslt = s.hasNext() ? s.next() : "";
    scanner.close();
    s.close();
    return rslt;
	}
	/**
	 * <span class="en">
	 * Initialise reverse encryptor with key
	 * </span>
	 * <span class="fr">
	 * Initialise le crypteur reversible avec la clé
	 * </span>
	 */
	private void initEncryptor(){
		textEncryptor = new StrongTextEncryptor();
		InputStream is = null;
		String key = "";
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.substring(0, path.lastIndexOf(System.getProperty("file.separator"))+1);
		File file = new File(path + KEYFILENAME);
		FileReader reader = null;
		try {
			if (file.exists()){
				reader = new FileReader(file);
	      BufferedReader buffer = new BufferedReader(reader);
	      String line = buffer.readLine();
	      while (line != null){
	      	key = line;
	      	if (!key.isEmpty()) break;
	      }
	      reader.close();
			}
			if (key.isEmpty()){
				is = getClass().getClassLoader().getResourceAsStream(KEYFILENAME);
				key = streamToString(is);
				is.close();
			}
			if (key.isEmpty()) key = DEFAULTKEY;
			textEncryptor.setPassword(key);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	public Encryption() {
		initEncryptor();
	}
	/**
	 * @param str
	 * <span class="en">
	 * Encrypte string
	 * </span>
	 * <span class="fr">
	 * Crypte un mot
	 * </span>
	 */
	public String encrypt(String str){
		return textEncryptor.encrypt(str);
	}
	/**
	 * @param str
	 * <span class="en">
	 * Uncrypte string
	 * </span>
	 * <span class="fr">
	 * Décrypte un mot
	 * </span>
	 */
	public String decrypt(String str){
		return textEncryptor.decrypt(str);
	}
	/**
	 * @param len
	 * <span class="en">
	 * Generate a random encryption key
	 * </span>
	 * <span class="fr">
	 * Génère une clé aléatoire pour le cryptage
	 * </span>
	 */
	public String getKey(int length){
		return randomString(length);
	}
}
